package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strconv"
)

const (
	example1File string = "input/test1.txt"
	inputFile    string = "input/input.txt"
)

func main() {

	iFile := getFlags()
	fileStr := getInputFile(iFile)

	dataMap, ttlMap, err := readInptFile(fileStr)
	if err != nil {
		fmt.Println(err)
	}

	if iFile > 0 {
		fmt.Println(dataMap)
		fmt.Println(ttlMap)
	}

	elf, calct, _ := getMaxCal(ttlMap)

	top1, top2, top3 := getTop3(ttlMap)

	fmt.Println("------------")

	fmt.Println(elf)
	fmt.Println(calct)

	fmt.Println("------------")

	fmt.Println(top1)
	fmt.Println(top2)
	fmt.Println(top3)
	fmt.Println(top1 + top2 + top3)
}

func getTop3(ttlMap map[int]int) (top1CalCt int, top2CalCt int, top3CalCt int) {

	for _, e := range ttlMap {

		if e > top3CalCt {
			if e > top2CalCt {
				if e > top1CalCt {
					top3CalCt = top2CalCt
					top2CalCt = top1CalCt
					top1CalCt = e
				} else {
					top3CalCt = top2CalCt
					top2CalCt = e
				}
			} else {
				top3CalCt = e
			}
		}
	}

	return top1CalCt, top2CalCt, top3CalCt
}

func getMaxCal(ttlMap map[int]int) (mxElf int, mxCalCt int, err error) {

	for idx, e := range ttlMap {
		if e > mxCalCt {
			mxCalCt = e
			mxElf = idx
		}
	}

	return mxElf, mxCalCt, nil
}

func getFlags() (iptFile int) {
	tmpFileFlag := flag.Int("e", 1, "What set of input to use, default 1 (example), prod is 0")
	flag.Parse()
	iptFile = *tmpFileFlag
	return iptFile
}

func getInputFile(iptFile int) string {
	switch iptFile {
	case 0:
		return inputFile
	case 1:
		return example1File
	default:
		return example1File
	}
}

func readInptFile(inFile string) (dataMap map[int][]int, ttlMap map[int]int, err error) {
	//initialize map
	dataMap = make(map[int][]int)
	ttlMap = make(map[int]int)

	inF, err := os.Open(inFile)
	if err != nil {
		return nil, nil, err
	}
	defer inF.Close()

	scanner := bufio.NewScanner(inF)
	var tmpIntArr []int
	elf := 1
	tmpCalCt := 0
	for scanner.Scan() {
		tmpStr := scanner.Text()
		if tmpStr != "" {
			cal, err := strconv.Atoi(tmpStr)
			if err != nil {
				return nil, nil, err
			}
			tmpCalCt = tmpCalCt + cal
			tmpIntArr = append(tmpIntArr, cal)
		}
		if tmpStr == "" {
			dataMap[elf] = tmpIntArr
			ttlMap[elf] = tmpCalCt
			elf++
			tmpCalCt = 0
		}
	}

	dataMap[elf] = tmpIntArr
	ttlMap[elf] = tmpCalCt

	inF.Close()

	return dataMap, ttlMap, nil
}
